-------------------------------
|-----------------------------|
|-- Argument Parser for PHP --|
|-----------------------------|
-------------------------------

PHP version : 7.1+
(use of void typed functions, nullable function arguments, private constantes)

5.1+ version available in 5_1_ArgParser.php
Please take note that the 5.1 version makes less typing checks.
Care about the type of variables you give to the parser with it.

----------------
| Installation |
----------------

Just put the file ArgParser.php in your project directory and import it in your PHP script.
No dependance needed.
<?php
require('ArgParser.php');
?>

Your server / CLI should fill the $argv variable to make the parser working. (You can still give to the parser a PHP-hand-made array)

-----------------
| Basic example |
-----------------

<?php
// Create the parser with standard $argv auto-filled arguments
$parser = new ArgParser();

// Adding multiple arguments
// First value is for user-entered arguments (ex : for "ls -a -l", recognized arguments are "a" and "l")
// Second value is for internal name of the argument (the name linked to the argument)
// You can set multiple user-entered arguments linked to one internal name

$parser->addArgument('v', 'verbose');
$parser->addArgument('t', 'test_mode');
$parser->addArgument(['s', 'save'], 'save');

// Building arguments (let the parser recognize which argument is set by the user)
$parser->buildArguments();

// Getting set arguments by its internals name
$verbose_mode = $parser->getArgument('verbose');
$test_mode = $parser->getArgument('test_mode');
$will_save = $parser->getArgument('save');

// At this point, if the user has entered (for example) "-v", $verbose_mode is (bool)true.
// If the user hasn't entered "-t", $test_mode will be (bool)false.
// Finally, if the user has entered "-s" or "--save", $will_save will be (bool)true.
?>

-------------------------------
-- ADDING FLAGS TO ARGUMENTS --

When you add an argument, you can set multiple parameters.

· You can precise specific flag usable only on one of the array of arguments (it will ignore the general flags defined).

<?php
$parser->addArgument([['v', ArgParser::ORPHAN], 'verbose'], 'verbose', ArgParser::VALUED);
// -v will be orphan, --verbose will wait a value
?>


- VALUED ARGUMENTS -

· By default, an argument is orphan (means that the argument isn't followed by a value, his presence means he's enabled).
You can specify that he's waiting for a user value (returned as string) by adding the flag ArgParser::VALUED.
Optionnaly, you can specify that the argument is orphan (but it's argument's normal behaviour) by flag ArgParser::ORPHAN.

$ php my_super_script.php -v Hello
<?php
$parser->addArgument('v', 'verbose', ArgParser::VALUED);
$parser->addArgument('o', 'other', ArgParser::VALUED);

$parser->buildArguments();

echo $parser->getArgument('verbose');
// Print "Hello" : the user entered value.

echo $parser->getArgument('other');
// Print nothing (not set => function returns false).
?>


· If you specify a flag with the third parameter of addArgument, he will apply to all user argument entered.

<?php
$parser->addArgument(['v', 'verbose'], 'verbose', ArgParser::VALUED);
// -v and --verbose are both waiting for a value after
?>


- DASHES -

· By default, arguments don't care about dashes (you can enter them like "v", "-v" or "--v", the parser don't check).
You can choose how the argument show be written with specific flags.

· If the argument should NOT have dashes before his name,
use the ArgParser::WO_DASH flag.

· If the argument should have ONLY ONE dash before,
use the ArgParser::WITH_DASH flag.

· If the argument MUST have TWO dash before,
use the ArgParser::WITH_DOUBLE_DASH flag.


- COMBINE FLAGS -

· You can combine flags to enable multiple features at once.
For example, if you want an argument that can be valued AND name written with one dash,
you can write :

<?php
$parser->addArgument('v', 'verbose', ArgParser::VALUED + ArgParser::WITH_DASH);
// -v wait a value after him (separated by a space) and should be written exactly like that
?>


· You can enable support for two differents typing at once,
If you want that your argument should be valued, and written with a dash, or two dashes,
you wan write :

<?php
$parser->addArgument('v', 'verbose', ArgParser::VALUED + ArgParser::WITH_DASH + ArgParser::WITH_DOUBLE_DASH);
// -v wait a value after him (separated by a space) and should be written "-v" or "--v".
?>


· Combined flags can by applied for one argument using the previous seen method.

<?php
$parser->addArgument([['v', ArgParser::VALUED + ArgParser::WITH_DASH], ['verbose', ArgParser::VALUED + ArgParser::WITH_DASH]], 'verbose');
// -v is defined and valued
// --verbose is defined and valued
?>


----------------------
| Full Documentation |
----------------------

---------------------------------------------------------
-- ArgParser::__construct(?array $arg = null) : object --

if $arg is null, global variable $argv is used instead.
$arg must be an array of string.
String will be read starting by the [1] index.


---------------------------------------------------------------------------------------------------------------------------
-- ArgParser::addArgument((string | array) $arg_user_names, string $internal_name, int $mode = ArgParser::ORPHAN) : void --

Takes one (string) or multiple (array of strings) arguments assigned to one internal name.
Flags should be added in $mode : By default, an argument is orphan and accept all write types (no, one or two dashes).
You can combine flag with + operator between constants.
Flag constants are :
ORPHAN (default, authorize all write types)
VALUED (valued argument (wait a value after him), authorize all write types)
WO_DASH (authorize only argument name with any dash)
WITH_DASH (authorize only argument name with one dash)
WITH_DOUBLE_DASH (authorize only argument name with two dashes)

$arg_user_names should be a single string (one argument specified associated to $internal_name),
an array of string (all strings will be valid arguments associated to $internal_name),
an array of array composed by [$arg_name, $arg_mode] (if this composition is used, this $arg_name will ignore global $mode and use $arg_mode instead).


-------------------------------------------------------------
-- ArgParser::deleteArgument(string $arg_user_name) : void --

Delete an argument defined by his user value (not his internal name !)


---------------------------------------------------------------
-- ArgParser::getArgument(string $arg_internal_name) : mixed --

Must be used after using buildArguments() and setting all the valid arguments with setArgument().
Return true if an orphan argument identified by internal name $arg_internal_name is set by the user.
Return string value of an valued argument identified by internal name $arg_internal_name if he's been set by the user.
Return false if argument is not set.


-------------------------------------------------------------------------
-- ArgParser::setArgumentMode(string $arg_user_name, int $mode) : void --

Set a specific mode to an argument identified by his user name.
Erase previous mode value.


-----------------------------------------------------------------------------------
-- ArgParser::setGlobalArgumentMode(string $arg_internal_name, int $mode) : void --

Set a specific mode to arguments identified by their internal name.
Erase all previous modes values.


----------------------------------------
-- ArgParser::resetArguments() : void --

Reset all computed and set arguments.
You need to build again arguments using ArgParser::buildArguments() after reseting args.


--------------------------------------------------------------
-- ArgParser::resetUserArguments(?array $arg = null) : void --

Reset user defined arguments array.
You need to pass an valid argument array the same way you do in constructor.
If $arg is null, $argv will be used instead.
After this operation, consider reseting arguments using resetArguments() and build again.


----------------------------------------
-- ArgParser::buildArguments() : void --

Makes match user-defined arguments with arguments setted via setArgument.
You MUST use buildArguments() BEFORE using getArguments, otherwise all you gets will return false !
